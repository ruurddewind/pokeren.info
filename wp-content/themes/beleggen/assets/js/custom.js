jQuery(document).ready(function( $ ) {

    $(".open-btn").click(function() {

        $(".backdrop").toggleClass("active");
        $(".main-nav").toggleClass("active");

    });

    $(".close-btn").click(function() {
        $(".backdrop").removeClass("active");
        $(".main-nav").removeClass("active");
    })

});
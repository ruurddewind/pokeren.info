<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?php wp_head(); ?>

	<link rel="alternate" hreflang="nl_NL" href="<?php bloginfo('url'); ?>" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
</head>

<body>

<div class="container">
	<div class="row">
		<div class="col-12 p0">

			<main class="white-bg box-shadow">

				<header>
					<div class="row">
						<div class="col-12">
							<div class="header-top d-flex align-items-center justify-content-between">
								<a class="logo" href="<?php echo get_home_url(); ?>">
									<?php
										$custom_logo_id = get_theme_mod('custom_logo');
										$logo = wp_get_attachment_image_src($custom_logo_id , 'full');
										echo '<img src="'. (!empty($logo[0]) ? $logo[0] : '') .'" alt="'. get_bloginfo('title') .'" />';
									?>
								</a>
								<div class="search-bar d-none d-lg-block">
									<input type="search" name="" value="" />
								</div>
								<div class="open-btn d-flex d-lg-none">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon-hamburger.png" alt="Open menu" />
								</div>
							</div>
							<nav class="main-nav">
								<div class="close-btn d-flex justify-content-end d-lg-none">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon-close.png" alt="Sluit menu" />
								</div>
								<?php header_menu(); ?>
							</nav>
						</div>
					</div>
				</header>

				<?php get_template_part('partials/header-banner'); ?>
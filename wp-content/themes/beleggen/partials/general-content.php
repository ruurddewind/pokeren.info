<section class="main-content">
	<div class="row">

		<div class="col-12 col-lg-8">
			<section class="content">
				<?php

					if (is_category()) {

						// query arguments
						$cat_args  = [
							'orderby'       => 'date',
							'order'         => 'DESC',
							'category_name' => single_cat_title( '', false )
						];
						$cat_posts = new WP_Query( $cat_args );

						// loop trough categories
						if ( $cat_posts->have_posts() ) {
							while ( $cat_posts->have_posts() ) {
								$cat_posts->the_post();

								echo '<a href="'. get_permalink() .'">'. the_title('<h1>','</h1>',false) .'</a>';
								echo (get_the_date() ? '<div class="post-date">'. get_the_date() .'</div>' : '');

								// show excerpt
								the_excerpt();

							}
						}
						wp_reset_postdata();
					}


					// loop trough posts
					if (have_posts() && !is_category()) {
						while (have_posts()) {
							the_post();

							// only for post overview and post detail
							if(is_home() || is_single() || is_category()) {
							    echo ((is_home()) ? '<a href="'. get_permalink() .'">' : '') . the_title('<h1>','</h1>',false) . (is_home() ? '</a>' : '');
							    echo (get_the_date() ? '<div class="post-date">'. get_the_date() .'</div>' : '');
	                        }

							// show excerpt only on the post overview
							if (is_home()) {
								the_excerpt();
							} else {
								the_content();
							}

						}
					}

				?>
			</section>
		</div>

		<?php
			// determine the page/post id
			$page_id = (is_home() ? get_queried_object_id() : get_the_id());

			if (get_field('sidebar_widgets',$page_id)) {
				?>
				<div class="col-12 col-lg-4">
					<?php get_template_part('partials/sidebar') ?>
				</div>
				<?php
			}
		?>

	</div>
</section>
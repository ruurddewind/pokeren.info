<aside class="sidebar">
	<?php
		// top sidebar
		if (is_active_sidebar('widget-sidebar-top')) {
			dynamic_sidebar('widget-sidebar-top');
		}

		// bottom sidebar
		if (is_active_sidebar('widget-sidebar-bottom')) {
			dynamic_sidebar('widget-sidebar-bottom');
		}
	?>
</aside>
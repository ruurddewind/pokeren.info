<?php

	$banner_img = get_field('header_banner_img');
	$header_link = get_field('header_banner_link');

	if (get_field('header_banner') && (!empty($banner_img))) {
		?>
		<div class="header-banner-img">
			<?php echo (!empty($header_link) ? '<a href="'. $header_link["url"] .'" target="'. (!empty($header_link["target"]) ? $header_link["target"] : '_self') .'">' : ''); ?>
			<img src="<?php echo $banner_img['url']; ?>" alt="" />
			<?php echo (!empty($header_link) ? '</a>' : ''); ?>
		</div>
		<?php
	}
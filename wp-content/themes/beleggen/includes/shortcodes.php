<?php

    // button shortcode
	function styled_beleggen_button($attr){
		//shortcode attributes
		$args = shortcode_atts(array(
			'url'   => '#',
			'title' => 'Button tekst',
			'size'  => 'medium'
		), $attr);
		//output html
		$output = '<div class="button-holder"><a class="button '. $args['size'] .'" href="'. $args['url'] .'">'. $args['title'] .'</a></div>';
		return $output;
	}
	add_shortcode('button', 'styled_beleggen_button');


	// alert shortcode
    function styled_beleggen_alert($attr){
        //shortcode attributes
        $args = shortcode_atts(array(
            'title' => 'Alert tekst',
            'color'  => 'blue'
        ), $attr);
        //output html
        $output = '<div class="alert-holder '. $args['color'] .'">'. $args['title'] .'</div>';
        return $output;
    }
    add_shortcode('alert', 'styled_beleggen_alert');
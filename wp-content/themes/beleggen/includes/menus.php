<?php

	// register menu
	function register_menus() {
		register_nav_menus(
			array(
				'header-menu' => __('Hoofdnavigatie', get_template()),
				'footer-menu' => __('Footernavigatie', get_template())
			)
		);
	}
	add_action('init', 'register_menus');

	// create nav_menu
	function header_menu() {
		$menu_args = array(
			'menu'              => 'Hoofdmenu',
			'menu_class'        => 'navbar-nav',
			'theme_location'    => 'header-menu',
			'container'         => 'false'
		);
		wp_nav_menu($menu_args);
	}

    function footer_menu() {
        $menu_args = array(
            'menu'              => 'Footermenu',
            'menu_class'        => 'footer-nav',
            'theme_location'    => 'footer-menu',
            'container'         => 'false'
        );
        wp_nav_menu($menu_args);
    }
<?php
	// remove junk from head
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'feed_links', 2);
	remove_action('wp_head', 'index_rel_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'feed_links_extra', 3);
	remove_action('wp_head', 'start_post_rel_link', 10, 0);
	remove_action('wp_head', 'parent_post_rel_link', 10, 0);
	remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
	remove_action('wp_head', 'rest_output_link_wp_head', 10 );
	remove_action('wp_head', 'wp_oembed_add_discovery_links', 10 );

	add_filter('xmlrpc_enabled', '__return_false');

	add_filter( 'wpseo_metabox_prio', function() { return 'low';});

	function wpdocs_dequeue_script() {
		wp_dequeue_script( 'jquery' );
		wp_dequeue_script( 'jquery-ui-core' );
	}
	add_action( 'wp_print_scripts', 'wpdocs_dequeue_script', 100 );
	//add_filter( //'wpcf7_load_js', '__return_false' );
	//add_filter( 'wpcf7_load_css', '__return_false' );

	function my_jquery_enqueue() {
		wp_deregister_script('jquery');
	}
	if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);

	//remove menu items
	//function remove_admin_menu_items() {
	//	//remove_menu_page('edit.php');
	//	$user_data = wp_get_current_user();
//
	//	if ( $user_data->user_login != 'ruurd') {
	//		remove_menu_page('tools.php');
	//		remove_menu_page('edit.php?post_type=acf-field-group');
	//	}
	//}
	//add_action('admin_menu', 'remove_admin_menu_items');
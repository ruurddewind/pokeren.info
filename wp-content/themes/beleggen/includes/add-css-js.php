<?php

	//adding the css and js
	function add_scripts() {
		/*css*/
		wp_enqueue_style('bootstrap', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css');
		wp_enqueue_style('main', get_template_directory_uri() . '/assets/scss/style.css');

		/*js*/
		wp_enqueue_script('bootstrap', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js', array(), '', true);
		wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-2.2.4.min.js', array(), '', true);
		wp_enqueue_script('custom-js', get_template_directory_uri() . '/assets/js/custom.js', array(), '', true);
	};
	add_action('get_footer', 'add_scripts');
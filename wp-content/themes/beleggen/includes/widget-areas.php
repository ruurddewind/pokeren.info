<?php

	//widget sidebar for nav elements
	function sidebar_widget() {
		register_sidebar(
			array(
				'name'          => __('Sidebar widgetruimte boven',get_template()),
				'description'   => 'De bovenste sidebar widget',
				'id'            => 'widget-sidebar-top',
				'before_widget' => '<div class="sidebar top">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="sidebar-title">',
				'after_title'   => '</h3>'
			)
		);

		register_sidebar(
			array(
				'name'          => __('Sidebar widgetruimte onder',get_template()),
				'description'   => 'De onderste sidebar widget',
				'id'            => 'widget-sidebar-bottom',
				'before_widget' => '<div class="sidebar bottom">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="sidebar-title">',
				'after_title'   => '</h3>'
			)
		);
	}
	add_action('widgets_init', 'sidebar_widget');
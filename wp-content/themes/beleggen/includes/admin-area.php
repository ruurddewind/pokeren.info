<?php

	//add custom css to admin area
	function load_admin_styles() {
		wp_enqueue_style( 'admin_css', get_template_directory_uri() . '/assets/scss/admin.css', false, '1.2.0' );
	}
	add_action( 'admin_enqueue_scripts', 'load_admin_styles' );

	//customize footer text in admin area
	function admin_area_footer () { ?>
		<style>
			small.small-block {
				color: white;
				border-radius: 3px;
				padding: 0 4px 1px 4px;
				margin-right: 3px;
			}

			small.small-block.php {
				background-color: #8892BF;
			}

			small.small-block.sql {
				background-color: #2F99A3;
			}
		</style>
		<?php
		echo get_bloginfo('title') .' - '. get_bloginfo('description') .'<br /><small>Technische realisatie door <a href="https://www.linkedin.com/in/dewind/" rel="nofollow" target="_blank">Ruurd de Wind</a></small> / <small class="small-block php">PHP v'. phpversion() .'</small><small class="small-block sql">SQL v'. mysqli_get_client_info() .'</small>';
	}
	add_filter('admin_footer_text', 'admin_area_footer');

	//hide the default selectable template
	function standaardtemplate_filter_gettext( $translation, $text, $domain ) {
		if ( $text == 'Default Template' ) {
			return __( 'Standaard', 'standaard' );
		}
		return $translation;
	}
	add_filter( 'gettext', 'standaardtemplate_filter_gettext', 10, 3 );

	//show images in library bugfix
	//function stop_heartbeat() {
	//	wp_deregister_script('heartbeat');
	//}
	//add_action( 'init', 'stop_heartbeat', 1 );

	//add menu items to settings
	add_theme_support('menus');

	//add title attribute for wp_title
	add_theme_support('title-tag');

	//developer help via dashboard
	function custom_dashboard_widgets() {
		global $wp_meta_boxes;
		wp_add_dashboard_widget('custom_help_widget', 'Thema ondersteuning', 'custom_dashboard_help');
	}
	function custom_dashboard_help() {
		$theme_name = get_bloginfo('title');
		$theme_desc = get_bloginfo('description');
		$theme_url = get_bloginfo('url');
		echo '<p>Welkom bij het thema van '.$theme_name.' | '.$theme_desc.'. Indien je hulp nodig hebt kun je de developer van dit thema contacteren via <a href="mailto:geoclaps@hotmail.com">geoclaps@hotmail.com</a> of via <a href="tel:0646324126">06 46 32 41 26</a>.</p><p>Dit thema is ontwikkeld t.b.v. het domein '.$theme_url.'</p>';
	}
	add_action('wp_dashboard_setup', 'custom_dashboard_widgets');

	//adding custom stylesheet to admin area so we can style the tinymce editor
	add_filter('tiny_mce_before_init', function ($mce_init) {

		$content_css = get_stylesheet_directory_uri() .'/assets/css/admin.css';

		//Grab existing stylesheets and then add our new $content_css
		if ( isset( $mce_init[ 'content_css' ] ) ) {
			$content_css_new =  $mce_init[ 'content_css' ].','.$content_css;
		}

		$mce_init[ 'content_css' ] = $content_css_new;

		return $mce_init;
	});

	function hide_acf_menu() {
		$current_user = wp_get_current_user()->data->user_login;

		if ($current_user != 'ruurd') {
			?>
			<style>
				#toplevel_page_edit-post_type-acf-field-group { display: none; }
			</style>
			<?php
		}

	}
	add_action('admin_init','hide_acf_menu');

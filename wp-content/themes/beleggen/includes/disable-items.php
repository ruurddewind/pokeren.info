<?php

	//remove welcome dashboard panel
	remove_action('welcome_panel', 'wp_welcome_panel');
	
	function disable_default_dashboard_widgets() {
		global $wp_meta_boxes;
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
		// yoast seo
		unset($wp_meta_boxes['dashboard']['normal']['core']['yoast_db_widget']);
	}
	add_action('wp_dashboard_setup', 'disable_default_dashboard_widgets', 999);
	
	
	// disable comments comments type
	function disable_comments_post_types_support() {
		$post_types = get_post_types();
		foreach ($post_types as $post_type) {
			if(post_type_supports($post_type, 'comments')) {
				remove_post_type_support($post_type, 'comments');
				remove_post_type_support($post_type, 'trackbacks');
			}
		}
	}
	add_action('admin_init', 'disable_comments_post_types_support');
	
	// Close comments on the front-end
	function disable_comments_status() {
		return false;
	}
	add_filter('comments_open', 'disable_comments_status', 20, 2);
	add_filter('pings_open', 'disable_comments_status', 20, 2);
	
	
	// Hide existing comments
	function disable_comments_hide_existing_comments($comments) {
		$comments = array();
		return $comments;
	}
	add_filter('comments_array', 'disable_comments_hide_existing_comments', 10, 2);
	
	
	// Redirect any user trying to access comments page
	function disable_comments_admin_menu_redirect() {
		global $pagenow;
		if ($pagenow === 'edit-comments.php') {
			wp_redirect(admin_url());
			exit;
		}
	}
	add_action('admin_init', 'disable_comments_admin_menu_redirect');
	
	// Remove comments metabox from dashboard
	function disable_comments_dashboard() {
		remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
	}
	add_action('admin_init', 'disable_comments_dashboard');
	
	
	// Remove comments links from admin bar
	function disable_comments_admin_bar() {
		if (is_admin_bar_showing()) {
			remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
		}
	}
	add_action('init', 'disable_comments_admin_bar');
	
	// Disable menu items
	function remove_menus(){
		//remove_menu_page( 'index.php' );					//Dashboard
		remove_menu_page('edit-comments.php');			    //Comments
		//remove_menu_page( 'jetpack' );					//Jetpack*
		//remove_menu_page( 'edit.php' );					//Posts
		//remove_menu_page( 'upload.php' );					//Media
		//remove_menu_page( 'edit.php?post_type=page' );	//Pages
		//remove_menu_page( 'themes.php' );					//Appearance
		//remove_menu_page( 'plugins.php' );				//Plugins
		//remove_menu_page( 'users.php' );					//Users
		//remove_menu_page( 'tools.php' );					//Tools
		//remove_menu_page( 'options-general.php' );		//Settings
	}
	add_action( 'admin_menu', 'remove_menus' );
	
	// disable emoticons without plugin
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	
	// disable embedded files that come with Wordpress
	function disable_embeds_init() {
		// remove the REST API endpoint
		remove_action('rest_api_init', 'wp_oembed_register_route');
		// turn off oEmbed auto discovery, don't filter oEmbed results
		remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);
		// remove oEmbed discovery links
		remove_action('wp_head', 'wp_oembed_add_discovery_links');
		// remove oEmbed-specific JavaScript from the front-end and back-end
		remove_action('wp_head', 'wp_oembed_add_host_js');
	}
	add_action('init', 'disable_embeds_init', 9999);
	
	// disable emoticons without plugin
	function disable_emojis() {
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_action( 'admin_print_styles', 'print_emoji_styles' );
		remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
		remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
		remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
		add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
	}
	add_action( 'init', 'disable_emojis' );
	
	// disable emoticons from TinyMCE
	function disable_emojis_tinymce( $plugins ) {
		if ( is_array( $plugins ) ) {
			return array_diff( $plugins, array( 'wpemoji' ) );
		} else {
			return array();
		}
	}
	
	//disable plugin and theme editor
	//define( 'DISALLOW_FILE_EDIT', true );
	
	// disable gutenberg editor for posts and pages
	add_filter('use_block_editor_for_post', '__return_false', 10);
	add_filter('use_block_editor_for_post_type', '__return_false', 10);
	
	// disable installation of plugins and themes
	//define('DISALLOW_FILE_MODS',true);

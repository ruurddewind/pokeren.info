<?php

	// altered excerpt
	function altered_excerpt($more) {
		return ' ... [<a href="'. get_permalink() .'">'. __('Lees meer',get_template()) .'</a>]';
	}
	add_filter('excerpt_more','altered_excerpt');
				<footer>
					<div class="row">
						<div class="col-12">
							<nav>
								<?php footer_menu(); ?>
							</nav>
							<div class="bottom-text">
								<p>
									Copyright 2012 - <?php echo date('Y'); ?> © alle rechten voorbehouden. Beleggen.info is een initiatief van Site Diensten KvK: 55807410<br />
									Beleggen brengt het risico op verlies met zich mee. Verdiep je altijd in de risico's!
								</p>
							</div>
						</div>
					</div>
				</footer>

			</main>
		</div>
	</div>
</div>

<div class="backdrop">
</div>

<?php wp_footer(); ?>

</body>

</html>
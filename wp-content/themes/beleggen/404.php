<?php get_header(); ?>

<div class="clearfix"></div>

<div class="row">
	<div class="container text-center">
		<div class="col-md-24">
			<div class="page-not-found-holder">
				<h1>404 - Pagina niet gevonden</h1>
				<div>Helaas de pagina die u zoekt is niet gevonden.</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>